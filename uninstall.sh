#!/bin/bash
dialog --title "Uninstall CPL410-Demos" \
--yesno "This will remove everyting from your home directory that was installed by the installer.  We leave the system software installed by apt in-tact on your system.  If you wish to remove this software as well, perform a factory reset using the OLED display after running this uninstall.\n\nif you do not want to uninstall the CPL410-Demos, please chose No below." 13 60
response=$?
case $response in 
    0)
        clear
        cd ~
        pm2 stop logger 2>/dev/null
        pm2 delete logger 2>/dev/null
        pm2 stop pm2-http-interface 2>/dev/null
        pm2 delete pm2-http-interface 2>/dev/null
        rm -R cpl410-demos 2>/dev/null
        rm -R installed-demos 2>/dev/null
        rm -R .cache 2>/dev/null
        rm -R .npm 2>/dev/null
        rm -R .ansible 2>/dev/null
        rm -R .nano 2>/dev/null
        rm -R .pm2 2>/dev/null
        rm -R .cache 2>/dev/null
        rm -R .local 2>/dev/null
        rm -R .config 2>/dev/null
        rm -R .node-red 2>/dev/null
        rm -R .node-gyp 2>/dev/null
        rm .influx_history 2>/dev/null
        ;;
    1)
        clear
        echo Cancelling uninstall ...
        exit
        ;;
esac

