{* Smarty *}
<!DOCTYPE HTML>
<!--
	Photon by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>CPL410 Demos by Gescan/Cimtec</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Header -->
			<section id="header">
				<div class="inner">
					<span class="icon major fa-cloud"></span>
					<h1>CPL410-Demos</h1>
					<p>Welcome to the CPL410-Demos.  These applications are designed to give you an idea <br/>
                    on what can be done with the IC695CPL410.  They are brought to you by <a href="https://www.gescanautomation.com">Gescan Automation</a><br /> 
                    and <a href="https://www.cimtecautomation.com">Cimtec Automation</a></p>
					<ul class="actions special">
						<li><a href="#one" class="button scrolly">Discover</a></li>
					</ul>
				</div>
			</section>

		<!-- One -->
			<section id="one" class="main style1">
				<div class="container">
					<div class="row gtr-150">
						<div class="col-6 col-12-medium">
							<header class="major">
								<h2>Discover the CPL410</h2>
							</header>
							<p>Founded on the premise that a connected controller, like connected people, can take more intelligent actions, IICS is 
                            a modular, flexible control platform that leverages rich data and analytics to turn insights into action. With IICS,
                            customers can bring data to analytics that run in the cloud or bring analytics close to the machine with locally hosted apps.</p>
						</div>
						<div class="col-6 col-12-medium imp-medium">
							<span class="image fit"><img src="images/cpl410.jpg" alt="" /></span>
						</div>
					</div>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="main style2">
				<div class="container">
					<div class="row gtr-150">
						<div class="col-6 col-12-medium">
							<ul class="major-icons">
								<li><span class="icon style1 major fa-code"></span></li>
								<li><span class="icon style2 major fa-bolt"></span></li>
								<li><span class="icon style3 major fa-camera-retro"></span></li>
								<li><span class="icon style4 major fa-cog"></span></li>
								<li><span class="icon style5 major fa-desktop"></span></li>
								<li><span class="icon style6 major fa-calendar"></span></li>
							</ul>
						</div>
						<div class="col-6 col-12-medium">
							<header class="major">
								<h2>Technologies in Use</h2>
							</header>
							<p>Apache - The apache webserver allows you to serve up static or dynamic webpages.  Several demo's utilize the apache webserver including this webpage.</p>
							<p>Python - Many of the demo's utilize python scripts to interface with various OPC-UA servers, webservices, and linux services.  The Alarm Server, Controller Health Check, SNMP, and Analytics all use python.</p>
							<p>MQTT - MQTT is a lightweight messaging protocol commonly used in IoT applications.  The Alarm Server application utilizes MQTT for server to browser communications.</p>
                            <p>InfluxDB - This is a TSDB (Time Series Database) and is the foundation of the internet of things.   Storing large amounts of information for use, graphing, and analytics.</p>
                            <p>Grafana - A graphing frontend designed to take data from numerous datasources and create simple to use, beautiful graphs.</p>
						</div>
					</div>
				</div>
			</section>

		<!-- Three -->
			<section id="three" class="main style1 special">
				<div class="container">
					<header class="major">
						<h2>Available Demos</h2>
					</header>
					<p>The following demos have been installed and are available for discovery:</p>
					<div class="row gtr-150">
					    {section name=installedApplications loop=$apps}
						<div class="col-4 col-6-medium col-12-small">
							<span class="image fit"><img src="images/{$apps[installedApplications].image}" alt="" /></span>
							<h3>{$apps[installedApplications].name}</h3>
							<p>{$apps[installedApplications].description}</p>
                            <ul class="actions special">
								{section name=linksSection loop=$apps[installedApplications].links}
								<li><a href="{$apps[installedApplications].links[linksSection].href}" class="button">{$apps[installedApplications].links[linksSection].hrefText}</a></li>
								{/section}
							</ul>
						</div>
						{/section}
					</div>
				</div>
			</section>

		<!-- Four -->
			<section id="four" class="main style2 special">
				<div class="container">
					<header class="major">
						<h2>Machine Edition Project</h2>
					</header>
					<p>Be sure to download the sample Machine Edition project into the controller for all the demo's to work properly.</p>
					<ul class="actions special">
						<li><a href="meproject.zip" class="button wide primary">Download</a></li>
					</ul>
				</div>
			</section>

		<!-- Footer -->
			<section id="footer">
              <div class="row">
                    <div class="col-6">
                    <h3>Gescan Automation</h3>
                    <ul class="icons">
                       <li><a href="https://www.gescanautomation.com" class="icon alt fa-globe"><span class="label">Website</span></a></li>
					   <li><a href="https://twitter.com/gescanontario?lang=en" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
					   <li><a href="https://www.facebook.com/GescanAutomation/" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                       <li><a href="https://www.linkedin.com/company/gescan-automation" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
					   <li><a href="https://bitbucket.org/gescancimtec/" class="icon alt fa-bitbucket"><span class="label">BitBucket</span></a></li>
				    </ul>
                </div>
                <div class="col-6">
                <h3>Cimtec Automation</h3>
                    <ul class="icons">
                       <li><a href="https://www.cimtecautomation.com/" class="icon alt fa-globe"><span class="label">Website</span></a></li>
					   <li><a href="https://twitter.com/cimtec" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
					   <li><a href="https://www.facebook.com/Cimtec-Automation-Control-168654186482054/" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                       <li><a href="https://www.linkedin.com/company/cimtec-automation-llc" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
					   <li><a href="https://bitbucket.org/gescancimtec/" class="icon alt fa-bitbucket"><span class="label">BitBucket</span></a></li>
				    </ul>
                </div>
              </div>

				<ul class="copyright">
					<li>&copy;2018  Gescan Automation/Cimtec Automation</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
				</ul>
			</section>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
