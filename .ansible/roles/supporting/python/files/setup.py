from setuptools import setup

setup(
    name='cplcommon',
    version='1.0',
    description='Common functions for the CPL410 suite of apps',
    author='Tom Behnke',
    py_modules=['cplcommon']
)