#!/usr/bin/python3
from cplcommon import getWoeId

def updateWoeId():
    woeid = getWoeId().split(',')
    print("Current WOEID =", woeid[0])
    print("Current units =", woeid[1])
    print("Current API Key =", woeid[2])
    newWoeid = input("Enter new WOEID [Blank to keep existing woeid]: ")
    newUnits = input("Enter 'c' or 'f' [Blank to keep existing units]: ")
    newApiKey = input("Enter your API Key [Blank to keep existing API Key]: ")
    if newWoeid != '' or newUnits != '' or newApiKey != '':
        if newWoeid == '':
            newWoeid = woeid[0]
        if newUnits == '':
            newUnits = woeid[1]
        if newApiKey == '':
            newApiKey = woeid[2]
        filename = 'weatherid'
        newData = newWoeid + ',' + newUnits + ',' + newApiKey
        outfile = open(filename, 'w')
        outfile.writelines(newData)
        outfile.close()


def main():
    getWoeId()
    updateWoeId()


if __name__ == '__main__':
    main()
