#!/usr/bin/python3

import argparse
import urllib
import json
import datetime
import sys
from dateutil.parser import parse
from influxdb import InfluxDBClient
from opcua import ua
from cplcommon import getlogin, getWoeId, setupClient

TIMEOFFSET = 4  # Temporary fix to handle time differences in Grafana

def main(host='localhost', port=8086):
    # Instantiate a connection to the InfluxDB.
    weatherData = getWeatherData()
    user, password = getlogin('influxdb')
    dbname = 'weather'
    utc_datetime = datetime.datetime.utcnow().isoformat()
    json_body = [
        {
            "measurement": "weather",
            "time": utc_datetime,
            "fields": {
                "location": weatherData['name'] + ", " + weatherData['sys']['country'],
                "temperature_units": weatherData['temperatureUnits'],
                "wind_units": weatherData['windUnits'],
                "temperature": int(weatherData['main']['temp']),
                "humidity": int(weatherData['main']['humidity']),
                "atmosphere_pressure": float(weatherData['main']['pressure']),
                "wind_speed": float(weatherData['wind']['speed']),
                "wind_direction": int(weatherData['wind']['deg']),
                "current_conditions": weatherData['weather'][0]['description'].title(),
            }
        }
    ]
    client = InfluxDBClient(host, port, user, password, dbname)
    client.write_points(json_body)
    updateController(weatherData)


def getWeatherData():
    baseurl = "https://api.openweathermap.org/data/2.5/weather?"
    woeid = getWoeId().split(',')
    if woeid[2] == 'not_set':
        sys.exit('WOEID Not Set')
    units = 'metric' if woeid[1] == 'c' else 'imperial'
    request_url = baseurl + urllib.parse.urlencode({'id': woeid[0]}) + "&" + \
        urllib.parse.urlencode({'appid': woeid[2]}) + "&" + \
        urllib.parse.urlencode({'units': units}) + "&mode=json"
    result = urllib.request.urlopen(request_url, data=None).read()
    data = json.loads(result.decode('utf-8'))
    data['temperatureUnits'] = 'Deg. C' if woeid[1] == 'c' else 'Deg. F'
    data['windUnits'] = 'KPH' if woeid[1] == 'c' else 'MPH'
    return data


def updateController(weatherData):
    myClient = setupClient()
    try:
        myClient.connect()
        locationTag = myClient.get_node("ns=2;s=WeatherData.location")
        temperatureUnitsTag = myClient.get_node("ns=2;s=WeatherData.units")
        windUnitsTag = myClient.get_node("ns=2;s=WeatherData.wind.units")
        temperatureTag = myClient.get_node("ns=2;s=WeatherData.temperature")
        humidityTag = myClient.get_node("ns=2;s=WeatherData.humidity")
        apTag = myClient.get_node("ns=2;s=WeatherData.atmosphericPressure")
        wsTag = myClient.get_node("ns=2;s=WeatherData.wind.speed")
        wdTag = myClient.get_node("ns=2;s=WeatherData.wind.direction")
        wcTag = myClient.get_node("ns=2;s=WeatherData.wind.chill")
        ccTag = myClient.get_node("ns=2;s=WeatherData.currentConditions")

        locationTag.set_value(weatherData['name'] + "," + weatherData['sys']
                              ['country'],
                              ua.VariantType.String)
        temperatureUnitsTag.set_value(weatherData['temperatureUnits'][-1],
                                      ua.VariantType.String)
        windUnitsTag.set_value(weatherData['windUnits'], ua.VariantType.String)
        temperatureTag.set_value(int(weatherData['main']['temp']),
                                 ua.VariantType.Int16)
        humidityTag.set_value(int(weatherData['main']['humidity']),
                              ua.VariantType.Int16)
        apTag.set_value(float(weatherData['main']['pressure']),
                        ua.VariantType.Float)
        wsTag.set_value(float(weatherData['wind']['speed']),
                        ua.VariantType.Float)
        wdTag.set_value(int(weatherData['wind']['deg']),
                        ua.VariantType.Int16)
        ccTag.set_value(int(weatherData['weather'][0]['id']),
                        ua.VariantType.Int16)

    finally:
        myClient.disconnect()
        return 1


def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='example code to play with InfluxDB')
    parser.add_argument('--host', type=str, required=False,
                        default='localhost',
                        help='hostname of InfluxDB http API')
    parser.add_argument('--port', type=int, required=False, default=8086,
                        help='port of InfluxDB http API')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    main(host=args.host, port=args.port)
