If you want to add a function to cplcommon, just cut and paste your function into
".ansible/roles/supporting/python/templates/cplcommon.py.j2".

For proper documentation purposes, format your function like this:

def myFunc(number:int) -> float:
    """Returns a float from a given int."""
    return float(number)

If you want to test your addtion to cplcommon on your local system, do the following:
1) Add your function to python/cplcommon.py
2) Run 'python3 setup.py sdist'
3) cd into python/dist
4) Run 'sudo python3 -m pip install cplcommon-1.0.tar.gz'
5) Test your script to make sure the function you added works as expected.  
    You can just 'import cplcommon' or 'from cplcommon import myFunc' as
    you would any other module.