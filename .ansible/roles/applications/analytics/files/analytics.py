#!/usr/bin/python3

# import libraries
import argparse
import pandas as pd
from influxdb import DataFrameClient
from cplcommon import getlogin


def main(host='localhost', port=8086):
    """Instantiate a connection to the InfluxDB."""
    user, password = getlogin('influxdb')
    dbname = 'chc'
    protocol = 'json'
    client = DataFrameClient(host, port, user, password, dbname)

    print("Read DataFrame")
    datasets = client.query("select * from Sweep_Time LIMIT 1000")
    dataset = datasets['Sweep_Time']
    print("\nCount")
    print(dataset.count())
    print("\nSum")
    print(dataset.sum())
    print("\nMean")
    print(dataset.mean())
    print("\nMean Absolute Deviation")
    print(dataset.mad())
    print("\nMedian")
    print(dataset.median())
    print("\nMin")
    print(dataset.min())
    print("\nMax")
    print(dataset.max())
    print("\nMode")
    print(dataset.mode())
    print("\nProduct of Values")
    print(dataset.prod())
    print("\nStandard Deviation")
    print(dataset.std())
    print("\nUnbiased Variance")
    print(dataset.var())
    print("\nStandard Error of Mean")
    print(dataset.sem())
    print("\nSample Skewness")
    print(dataset.skew())
    print("\nSample Kurtosis")
    print(dataset.kurt())
    print("\nSample Quantile")
    print(dataset.quantile())


def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='example code to play with InfluxDB')
    parser.add_argument('--host', type=str, required=False,
                        default='localhost',
                        help='hostname of InfluxDB http API')
    parser.add_argument('--port', type=int, required=False, default=8086,
                        help='port of InfluxDB http API')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    main(host=args.host, port=args.port)
