#!/usr/bin/python3

import argparse
import urllib.request
import requests
import json 

apiUrl = 'https://floating-journey-90889.herokuapp.com/'
headers = {'Content-Type': 'application/json'}

def main():
    # Collect our IP Address / LAT,LNG / SerialNumber
    ip, lat, lng = collectLocationData()
    serialNumber = collectSerialNumber()
    if not serialNumber:
        raise Exception('Unable to retrieve Serial Number')

    webId = getWebId(serialNumber)
    if webId is None:
        createController(serialNumber, ip, lat, lng)
    else:
        updateController(webId, serialNumber, ip, lat, lng)

def collectLocationData():
    with urllib.request.urlopen("https://geoip-db.com/json") as url:
        data = json.loads(url.read().decode())
        return data['IPv4'], data['latitude'], data['longitude']

def collectSerialNumber():
    with open ('/var/ge/FieldAgentInfo/HwInfo.txt', 'rt') as myFile:
        for myLine in myFile:
            if myLine.__contains__('Serial'):
                return myLine.split(":")[1].strip()
        return False

def createController(serialNumber, ip, lat, lng):
    postUrl = '{0}controllers'.format(apiUrl)
    postVars = {'serialNumber': serialNumber, 'ip': ip, 'lat': lat, 'lng': lng}
    response = requests.post(postUrl, headers=headers, json=postVars)
    if response.status_code == 409:
        #not sure why we tried to create instead of update
        print('Recieved 409 while attempting to create controller')
        return None
    elif response.status_code == 400:
        #failed validation
        print(response.content)
        return None
    elif response.status_code == 201:
        #success
        return True

def getWebId(serialNumber):
    getUrl = '{0}controllers/serial/{1}'.format(apiUrl, serialNumber)
    response = (requests.get(getUrl, headers=headers))
    if response.status_code == 400:
        return None
    elif response.status_code == 200:
        return json.loads(response.content.decode())[0]['id']

def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='Sample Python Script for Cloud REST API')
    return parser.parse_args()

def updateController(webId, serialNumber, ip, lat, lng):
    putUrl = '{0}controllers/{1}'.format(apiUrl, webId)
    putVars = {'serialNumber': serialNumber, 'ip': ip, 'lat': lat, 'lng': lng}
    response = requests.put(putUrl, headers=headers, json=putVars)
    if response.status_code == 409:
        #not sure why we tried to update instead of create
        print('Recieved 409 while attempting to update controller')
        return None
    elif response.status_code == 400:
        #failed validation
        print(response.content)
        return None
    elif response.status_code == 200:
        #success
        return True

if __name__ == '__main__':
    args = parse_args()
    main()