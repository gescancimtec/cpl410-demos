#!/usr/bin/env python3
'''Traces selected OPC-UA tag in a web browser

The purpose of this demo application is to show how to obtain numerical data
from published tags on the CPL410 and graph it using Plotly Dash.
Dash also creates a webserver which publishes this page allowing a connected
computer or phone to view the tag in a nicely rendered graph.
'''

import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import plotly
import random
import plotly.graph_objs as go
from collections import deque
from opcua import ua
import atexit
from cplcommon import get_ip, setupClient


def stopping():
    myClient.disconnect()
    print("Client Disconnected")

# Set up port, IP address, and how many data points we will save for the graph.
# Since we are sampling at 1/sec, 60 gives us a minute's worth of data.
PORT = 8050
ADDRESS = get_ip()

MAXSAMPLES = 60

myClient = setupClient()
myClient.connect()
atexit.register(stopping)
variablesnode = myClient.get_node(ua.NodeId(1001, 2))  # This is the root node containing all of the published tags of the connected CPL410
variables = variablesnode.get_children()  # variables is now a list of all published tags
varlist = []  # This list of dicts will hold all numeric published tags so we can build our dropdown menu
validTags = {}  # This dict will hold all numeric published tag names and their nodes for easy lookup
cpl410Name = 'CPL410 Live Trend Example'  # This will be the Title (H1) of the webpage.  Set as a dummy.
for v in variables:
    if v.get_browse_name().Name == 'ControllerName':  # If a tag called 'ControllerName' exists, use its value instead of the dummy.  This way the user can name his controller.
        cpl410Name = v.get_value()
        continue
    if v.get_data_type() == 12:  # If the data type is a String (data_type 12 per free OPCUA docs), ignore it.  We can't graph a string.
        continue
    tempDict = {}  # This and the next few lines builts a two entry dict that we append into a list for our dropdown element on our page.
    tempDict['label'] = v.get_browse_name().Name
    tempDict['value'] = v.get_browse_name().Name
    varlist.append(tempDict)
    validTags[v.get_browse_name().Name] = v  # Add the readable name and node for each tag as an entry in a dict.

activeVar = list(validTags.values())[0]  # Pick a variable so the app starts graphing immediately.  We are not guaranteed which one comes up.
X = deque(maxlen=MAXSAMPLES)  # Set our list collection to hold a maximum size.  If it gets one bigger, throw out the earliest one.
Y = deque(maxlen=MAXSAMPLES)
X.append(1)
sample = activeVar.get_value()
if sample is True:
    Y.append(1)
elif sample is False:
    Y.append(0)
else:
    Y.append(sample)
maxY = Y[0]  # Since we only have one element so far, its our biggest and smallest member
minY = Y[0]

# This point on is heavy on Plotly Dash. Google Plotly Dash Documentation for a
# better understanding of what is going on here.
app = dash.Dash(__name__)

app.css.config.serve_locally = True  # Set to False if there is no internet connection
app.scripts.config.serve_locally = True  # Set to False if there is no internet connection

app.layout = html.Div([
    html.H1(cpl410Name, style={'textAlign': 'center'}),
    dcc.Dropdown(
        id='ActiveTag',
        options=varlist,
        value=activeVar.get_browse_name().Name,
        multi=False
        ),
    dcc.Graph(id='live-update-graph', animate=True),
    html.H3(id='currentVal', style={'textAlign': 'center'}),
    html.H3(id='maximumVal', style={'textAlign': 'center'}),
    html.H3(id='minimumVal', style={'textAlign': 'center'}),
    dcc.Interval(
        id='interval-component',
        interval=1000,  # Update every 1000ms.  Faster doesn't seem to work.  In the real world, 1 second sampling is probably not useful anyway.
        n_intervals=0
        )
    ]
)


@app.callback([Output('live-update-graph', 'figure'),
              Output('currentVal', 'children'),
              Output('maximumVal', 'children'),
              Output('minimumVal', 'children')],
              [Input('interval-component', 'n_intervals'),
              Input('ActiveTag', 'value')])
def update_graph(n, currentVar):
    global maxY  # Had to use globals to pull in the initial sample from above.  There is probably a better way to do this.
    global minY
    global activeVar

    if activeVar.get_browse_name().Name == currentVar:
        X.append(X[-1]+1)
        sample = validTags[currentVar].get_value()
        if sample is True:
            Y.append(1)
        elif sample is False:
            Y.append(0)
        else:
            Y.append(sample)
        if maxY < Y[-1]:
            maxY = Y[-1]
        if minY > Y[-1]:
            minY = Y[-1]
    else:  # It's been changed; reset the graph and start sampling again.
        X.clear()
        Y.clear()
        X.append(1)
        sample = validTags[currentVar].get_value()
        if sample is True:
            Y.append(1)
        elif sample is False:
            Y.append(0)
        else:
            Y.append(sample)
        maxY = Y[0]
        minY = Y[0]
        activeVar = validTags[currentVar]

    data = go.Scatter(
        x=list(X),
        y=list(Y),
        name='Scatter',
        mode='lines+markers',
        fill='tozeroy',
        fillcolor='#98FB98'
        )

    return {'data': [data], 'layout': go.Layout(xaxis=dict(range=[min(X), max(X)]),
                                                yaxis=dict(range=[min(Y), max(Y)]))}, 'Current Value: ' + format(Y[-1], '.3f'), 'Maximum Value: ' + format(maxY, '.3f'), 'Minimum Value: ' + format(minY, '.3f')


if __name__ == "__main__":
    app.run_server(debug=False,
                   port=PORT,
                   host=ADDRESS)
