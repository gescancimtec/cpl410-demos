<?php
$curl = curl_init('https://floating-journey-90889.herokuapp.com/controllers');
curl_setopt_array($curl, [
    CURLOPT_RETURNTRANSFER => 1
]);
$response = json_decode(curl_exec($curl));
curl_close($curl);
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    
    <title>Cloud Example on CPL410</title>

    <style>
      #header {
        margin-bottom: 1em;
      }
    </style>
  </head>
  <body>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Gescan Automation</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="https://bitbucket.org/gescancimtec/cpl410-demos">Repository</a>
        <a class="p-2 text-dark" href="https://bitbucket.org/gescancimtec/cpl410-demos/issues?status=new&status=open">Issues</a>
      </nav>
    </div>

    <div class="px-3 py-3 pb-md-4 mx-auto text-center">
      <h1 class="display-4">CPL410 Cloud Example</h1>
      <p class="lead text-muted">This demo uses a cloud server runnign on heroku.com to collect IP Address and location data from CPL410's running the CPL410-Demos package.</p>
    </div>
    <div class="container">
      <div id="map" style="height: 600px; border: 1px solid #AAA; margin: 5em auto;"></div>
    </div>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type='text/javascript' src='/gescancimtec/assets/js/jquery.min.js'></script>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script>
      $(document).ready(function(){
        var mymap = L.map('map').setView([50, -20], 3);
        L.tileLayer('http://services.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
	        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
	        maxZoom: 18,
	        id: 'mapbox.streets'
        }).addTo(mymap);
        <?php
        foreach ($response as $item) {
          ?>
          var marker = L.marker([<?= $item->lat ?>, <?= $item->lng ?>]).addTo(mymap);
          marker.bindPopup("Serial Number: <?= $item->serialnumber ?><br/>IP Address: <?= $item->ip ?><br/>Last Updated: <?= $item->updated ?>");
          <?php
        }
        ?>
      });
    </script>
  </body>
</html>