import os
import argparse
import signal
import json
import time
import logging
import sys
import paho.mqtt.client as mqtt
from imutils.video import VideoStream
from imutils.video import FPS
from datetime import datetime
import face_recognition
import imutils
import pickle
import cv2
import sqlite3
from cplcommon import getlogin


class FacialRecognition:
    def __init__(self, args):
        logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                            stream=sys.stderr, level=args["logging"])

        logging.debug("Loading encodings + face detector...")
        self.data = pickle.loads(open(args["encodings"], "rb").read())
        self.detector = cv2.CascadeClassifier(args["cascade"])
        self.outfile = args["outfile"]

        self.interrupted = False
        signal.signal(signal.SIGINT, self.signalHandler)

    def mqttConnect(self, client, userdata, flags, rc):
        pass

    def mqttMessage(self, client, userdata, msg):
        pass

    def recogniseFaces(self, frame):
        # resize image to 500px (to speedup processing)
        frame = imutils.resize(frame, width=500)

        # convert the input frame from (1) BGR to grayscale (for face
        # detection) and (2) from BGR to RGB (for face recognition)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # detect faces in the grayscale frame
        rects = self.detector.detectMultiScale(gray, scaleFactor=1.1,
                                               minNeighbors=5, minSize=(30, 30))

        # OpenCV returns bounding box coordinates in (x, y, w, h) order
        # but we need them in (top, right, bottom, left) order, so we
        # need to do a bit of reordering
        boxes = [(y, x + w, y + h, x) for (x, y, w, h) in rects]

        # compute the facial embeddings for each face bounding box
        encodings = face_recognition.face_encodings(rgb, boxes)
        names = []

        # loop over the facial embeddings
        for encoding in encodings:
            # attempt to match each face in the input image to our known
            # encodings
            matches = face_recognition.compare_faces(self.data["encodings"],
                                                     encoding)
            name = "Unknown"

            # check to see if we have found a match
            if True in matches:
                # find the indexes of all matched faces then initialize a
                # dictionary to count the total number of times each face
                # was matched
                matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                counts = {}

                # loop over the matched indexes and maintain a count for
                # each recognized face face
                for i in matchedIdxs:
                    name = self.data["names"][i]
                    counts[name] = counts.get(name, 0) + 1

                # determine the recognized face with the largest number
                # of votes (note: in the event of an unlikely tie Python
                # will select first entry in the dictionary)
                name = max(counts, key=counts.get)

            # update the list of names
            names.append(name)

        # loop over the recognized faces
        for ((top, right, bottom, left), name) in zip(boxes, names):
            # draw the predicted face name on the image
            cv2.rectangle(frame, (left, top), (right, bottom),
                          (0, 255, 0), 2)
            y = top - 15 if top - 15 > 15 else top + 15
            cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
                        0.75, (0, 255, 0), 2)

        return (frame, names)

    def run(self):
        try:
            logging.debug("Opening sqlite database...")
            self.db = self.setupSql()

            if not os.path.exists('history'):
                logging.debug("Creating history folder ...")
                os.makedirs('history')

            logging.debug("Initializing MQTT...")
            user, password = getlogin('mosquitto')
            self.mqttClient = self.setupMqtt(user, password)
            self.mqttClient.connect("localhost")
            self.mqttClient.loop_start()

            # initialize the video stream and allow the camera sensor to warm up
            logging.debug("Starting video stream...")
            vs = VideoStream(src=-1)
            vs.start()
            time.sleep(2.0)

            # initialize our state variables
            previousNames = None

            while True:
                # grab the frame from the threaded video stream
                frame = vs.read()

                # recognise the faces in the frame, also get name array
                frame, names = self.recogniseFaces(frame)

                # remove unknowns from names array
                modifiedNames = [x for x in names if x != 'Unknown']

                # reset previousName if no names (allows same person back to back)
                if len(modifiedNames) == 0:
                    previousNames = None

                # compare name array against previous
                if len(modifiedNames) > 0 and ', '.join(modifiedNames) != previousNames:
                    previousNames = ', '.join(modifiedNames)
                    logging.debug("Detected: " + previousNames)
                    dateString = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
                    # push list of names to MQTT
                    self.mqttClient.publish("fr/names", json.dumps({'Date':
                                            dateString, 'Names': previousNames}))
                    # write list of names to history database
                    c = self.db.cursor()
                    entities = (dateString, previousNames)
                    c.executemany('INSERT INTO history(dateString, previousNames) VALUES(?, ?)', (entities,))
                    self.db.commit()
                    # write historical image to filesystem
                    cv2.imwrite("history/"+dateString+".png", frame)

                # update our real time image feed
                cv2.imwrite(self.outfile, frame)
                self.mqttClient.publish("fr/update", "update")
                time.sleep(0.25)

                if self.interrupted:
                    break
        finally:
            cv2.destroyAllWindows()
            vs.stop()
            self.mqttClient.loop_stop(force=False)
            self.mqttClient.disconnect()
            self.db.close()

    def setupMqtt(self, user, password):
        mqttClient = mqtt.Client()
        mqttClient.username_pw_set(user, password)
        mqttClient.on_connect = self.mqttConnect
        mqttClient.on_message = self.mqttMessage
        return mqttClient

    def setupSql(self):
        db = sqlite3.connect('historyDb')
        c = db.cursor()
        c.execute('CREATE TABLE IF NOT EXISTS history(dateString string, previousNames string)')
        db.commit()
        return db

    def signalHandler(self, signal, frame):
        self.interrupted = True


def parse_args():
    ap = argparse.ArgumentParser()
    ap.add_argument("-c", "--cascade", required=True,
                    help="path to where the face cascade resides")
    ap.add_argument("-e", "--encodings", required=True,
                    help="path to serialized db of facial encodings")
    ap.add_argument("-l", "--logging", choices=['DEBUG', 'INFO', 'WARNING',
                    'ERROR', 'CRITICAL'], default='INFO',
                    help="(Optionsl) Logging Level")
    ap.add_argument("-o", "--outfile", default="video.png",
                    help="file to write output to")
    group = ap.add_mutually_exclusive_group(required=True)
    group.add_argument("-v", "--video", action='store_true',
                       help="read images from default video stream")
    group.add_argument("-i", "--image",
                       help="image file to perform facial recognition against")
    return vars(ap.parse_args())

if __name__ == '__main__':
    args = parse_args()
    if args["video"]:
        FacialRecognition(args).run()
    else:
        frame, names = FacialRecognition(args).recogniseFaces(cv2.imread(args["image"]))
        cv2.imwrite(args["outfile"], frame)
